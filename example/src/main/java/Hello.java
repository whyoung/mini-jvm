public class Hello {
  public static void main(String[] args) {
    Hello hello = new Hello();

    System.out.println(hello.hashCode());
    run();
    int x = hello.run3();
    System.out.println(x);

    hello = new Hello();
    System.out.println(hello.hashCode());

    String s1 = "hello1";
    String s2 = "hello1";
    System.out.println(s1 == s2);
  }

  private static void run() {
    System.out.println("hello");
    run2();
  }

  private static void run2() {
    System.out.println("hello2");
  }

  private int run3() {
    return 3;
  }
}
