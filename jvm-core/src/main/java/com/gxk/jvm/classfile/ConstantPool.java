package com.gxk.jvm.classfile;

/**
 * 字节码常量池
 */
public class ConstantPool {
  public final ConstantInfo[] infos;

  public ConstantPool(int size) {
    this.infos = new ConstantInfo[size];
  }
}
