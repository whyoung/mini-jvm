package com.gxk.jvm.instruction;

import com.gxk.jvm.rtda.Frame;

public class IReturnInst implements Instruction {

  @Override
  public void execute(Frame frame) {
    int tmp = frame.popInt();
    frame.thread.popFrame();
    //对于有返回值的方法，获取操作数栈顶元素，再将前一个方法栈帧弹出，将返回值压入后面的栈帧的操作数栈中
    if (!frame.thread.empty()) {
      frame.thread.currentFrame().pushInt(tmp);
    }
  }

  @Override
  public String format() {
    return "ireturn";
  }
}
